public class SearchPokeController {
    
    public String searchName {get; set;}    
    public String pokemonId {get; set;}
    
    public PageReference redirect() {
        Pokemon__c pokemonId;
        PageReference searchResult;
        
        List<Pokemon__c> searchId = [SELECT Id, Name FROM Pokemon__c WHERE name =: searchName];
        if(searchId.size() > 0) {
            pokemonId = searchId[0];				            
        }
        
        if(pokemonId != null) {
            searchResult= new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+ pokemonId.Id);
            searchResult.setRedirect(true);               
        }        
        return searchResult;                  
    }  
}
