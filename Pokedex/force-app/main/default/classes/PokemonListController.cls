public with sharing class PokemonListController {
    
   public  static List<Pokemon__c> getPokemon()
    {
        List<Pokemon__c> results=[
            SELECT NAME, id, nature__c, ability__c, move1__c, move2__c, move3__c, move4__c
            FROM Pokemon__c
        ];
        return results;
    }
}
