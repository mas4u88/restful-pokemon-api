public with sharing class PokeApi {


    // ENUM data type for accessing a map with strings for significantly less hardcoding
    public enum ApiAccessor {MAIN, NAME, MOVE, ABIL, NATURE}
    private enum pokeVar {NAME, NATURE, SPECIES, ABILITY, MOVE1, MOVE2, MOVE3, MOVE4}

    // stores parts of the HTTPS:// endpoint we need to pass request in its .setEndpoint() method
    // always use MAIN, then add your accessor like NAME, then add the search restrictor.
    //
    // request.setEndpoint(pokeAcc.get(ApiAccessor.MAIN) + pokeAcc.get(ApiAccessor.NAME) + searchValue);

    private final Map<ApiAccessor, String> pokeAcc = new Map<ApiAccessor, String>
            {   ApiAccessor.MAIN => 'https://pokeapi.co/api/v2', 
                ApiAccessor.NAME => '/pokemon/', ApiAccessor.MOVE => '/move/', 
                ApiAccessor.ABIL => '/ability/', ApiAccessor.NATURE => '/nature/'};

    // stores the different values we need to set with identical Enum class to Pokemon
    // but you can't make Enums static, so we have to pass strings. to make for less
    // hardcoding, i've created an Enum and Map
    private final Map<pokeVar, String> pokeSetVars = new Map<pokeVar, String>{
        pokeVar.NAME => 'Name', pokeVar.Nature => 'Nature', pokeVar.SPECIES => 'Species',
        pokeVar.ABILITY => 'Ability', pokeVar.MOVE1 => 'Move1',
        pokeVar.MOVE2 => 'Move2', pokeVar.MOVE3 => 'Move3', pokeVar.MOVE4 => 'Move4'};

    // this is explicitly the JSON.deserialisedUntyped(response.getBody()) storage
    // this is NOT response.getBody(), this is the JSON string
    private static Map<String, Object> apiReturn;

    // there are only 25 possible natures in pokemon. constant.
    private static final Integer numNatures = 25;

    // Http variable setup for our HTTP request to the pokeapi
    private Http         http       = new Http();
    private HttpRequest  request    = new HttpRequest();
    private HttpResponse response   = new HttpResponse();

    // we are always only getting, so any time an Api Accessor is made, its method needs to be set
    public PokeApi() 
    {
        request.setMethod('GET');
    }

    // If we are handed only one string, that means the user did not deign to give the poke a nickname.
    // so we simply pass the value twice to our overloaded method that sets name and species to both
    public Pokemon getPokeByName(String pokeSpecies)
    {
        return getPokeByName(pokeSpecies, pokeSpecies);
    }

    // this where we call the API and get back a specific pokemon's data in JSON format.
    // we parse through it, get POSSIBLE values, then randomly assign values,
    // we then return a fully fledged pokemon
    public Pokemon getPokeByName(String pokeSpecies, String nickName)
    {
        // this is where we store all of our information
        // Pokemon has a constructor that sets the species and name
        Pokemon myNewPoke = new Pokemon(pokeSpecies, nickName);
        String lcName = pokeSpecies.toLowerCase();                  // the pokeapi.co only searches with lower case

        // create a somewhat dynamic API endpoint, this will look like https://pokeapi.co/api/v2/pokemon/[pokemon name]
        request.setEndpoint(pokeAcc.get(ApiAccessor.MAIN)+pokeAcc.get(ApiAccessor.NAME)+lcName);
        response = http.send(request);  // send and store response

        // if its a successful API call, work with the data
        if(response.getStatusCode() == 200)
        {
            // deserialise the response body, and cast it into a Map. the closest thing Apex has to JSON
            apiReturn = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
            
            // our api return has MANY different objects, right now we want to work with the abilities,
            // which contains a list of objects, that we then want to get all the names of to then randomly
            // assign to our pokemon
            // ABILITIES ARE LIMITED BY POKEMON SPECIES, POKEAPI RETURNS AN AUTOMATICALLY LIMITED SET OF ABILITIES
            List<Object> allPossibleAbilities = (List<Object>)apiReturn.get('abilities');
            List<String> abilityNames = new List<String>();

            // this is layers of complexity of casting to a String, Object map simply to parse through the JSON to get a name,
            // which we cast to a string, and store for use in a moment
            // we get sent a fair amount of "junk" data, like game-version and such. this is why we have get(abilities), then a foreach loop
            //
            //      (going down one layer of JSON)
            //
            //              and THEN a get(ability), which is also a Map, that we then have to cast, 
            //
            //                      and then get the name of the ability from inside
            for(Object abil : allPossibleAbilities)
            {
                abilityNames.add((String)((Map<String, Object>)(((Map<String, Object>)abil).get('ability'))).get('name'));
            }
            //System.debug(abilityNames);

            // repeat for moves, but instead of names, since we need 4 of them, used a SET of indices
            // MOVES ARE AUTOMATICALLY LIMITED BY POKEMON SPECIES, AUTOMATICALLY SENT LIMITED LIST BY API
            // that are in the bounds of our allPossibleMoves list
            List<Object> allPossibleMoves = (List<Object>)apiReturn.get('moves');

            Set<Integer> randMoveIndices = new Set<Integer>();
            // all pokemon can have a maximum of 4 moves. always. switch to final/constant
            while(randMoveIndices.size() < 4)
            {
                // add a new Integer to the set, randomly generated, with a ceiling of List.size()-1 (to account for 0 indexing)
                randMoveIndices.add(randNumUnderParam(allPossibleMoves.size()));
            }
            
            // actually set the ability we got earlier
            myNewPoke.setPokeVal(pokeSetVars.get(pokeVar.ABILITY), abilityNames[randNumUnderParam(abilityNames.size())]);

            // set the nature via calling getPokeNature, and passing a randomly generated Integer.
            // numNatures is 25, it is a constant. but since pokeapi is NOT 0 indexed, we need to add 1 to access
            // 1-25, not 0-24
            myNewPoke.setPokeVal(pokeSetVars.get(pokeVar.NATURE), getPokeNature(randNumUnderParam(numNatures)+1));

            // for every single random move index we have, set the move for it
            for(Integer i = 0; i < randMoveIndices.size(); i++)
            {
                switch on i 
                {
                    when 0
                    {
                        // set move 1, to whatever is stored in allPossibleMoves, in the index of i
                        myNewPoke.setPokeVal(pokeSetVars.get(pokeVar.MOVE1), (String)((Map<String,Object>)((Map<String, Object>)allPossibleMoves[i]).get('move')).get('name'));
                    }
                    when 1
                    {
                        // repeat ad-nauseum
                        myNewPoke.setPokeVal(pokeSetVars.get(pokeVar.MOVE2), (String)((Map<String,Object>)((Map<String, Object>)allPossibleMoves[i]).get('move')).get('name'));
                    }
                    when 2
                    {
                        myNewPoke.setPokeVal(pokeSetVars.get(pokeVar.MOVE3), (String)((Map<String,Object>)((Map<String, Object>)allPossibleMoves[i]).get('move')).get('name'));
                    }
                    when 3
                    {
                        myNewPoke.setPokeVal(pokeSetVars.get(pokeVar.MOVE4), (String)((Map<String,Object>)((Map<String, Object>)allPossibleMoves[i]).get('move')).get('name'));
                    }
                }
            }
            

        } // end if(response.getStatus() == 200)
        else
        {
            // if we fail our API call, we need to do something else here
            myNewPoke=null;
            System.debug('HHTP Response Error');
            System.debug(response.getBody());
        }

        // return our pokemon now with set variables
        return myNewPoke;
    }

    // this method takes in one integer, it will be 1-25, need to set validation
    public String getPokeNature(Integer natureApiIndex)
    {
        // the name of the nature we're actually going to get/store/return
        String natureName;

        // dynamically set request.Endpoint to https://pokeapi.co/api/v2/nature/]1-25]
        // then send request, and store response
        request.setEndpoint(pokeAcc.get(ApiAccessor.MAIN)+pokeAcc.get(ApiAccessor.NATURE)+(''+natureApiIndex));
        response = http.send(request);

        if(response.getStatusCode() == 200)
        {
            // take the response's body, deserialise it into JSON, then immediately access the names section of it,
            // by casting it to an map.
            // what we get back is a list, that's still more JSON deep
            List<Object> tempInner = (List<Object>)((Map<String, Object>)JSON.deserializeUntyped(response.getBody())).get('names');

            // the english language is in the location 7, so as a default, i put that. but this is not guaranteed.
            Integer enLocation = 7;

            // since 7 is not guaranteed to be the english language location, we dynamically find it
            for(Integer i = 0; i < tempInner.size(); i++)
            {
                // since our List is still technically one or 2 more levels of JSON deep,
                // we need to access one level lower, and we use the array/list accessor [i]
                // to automatically go through the list, and access lower,
                // where we cast to a map, and access the name OF THE LANGUAGE,
                // to guarantee we're getting the ENGLISH nature name
                /**
                 *      tempInner {
                 *          language {
                 *              name: 'en',
                 *              * junk data *
                 *          }
                 *          name = [Nature Name]
                 *      }
                 */

                 // but we make sure we don't store any of the excess travelling INTO the JSON, since
                 // what we want is not 2 levels lower, but only 1
                if(((Map<String, Object>)tempInner[i]).get('name') == 'en')
                {
                    // when we find the location of the english language, end the for loop
                    enLocation = i;
                    break;
                }
            }
            // since we did not store going down the JSON with the for loop,
            // but we did store the location of the english language,
            // we access it, cast it to a map, access teh NATURE name,
            // then cast that to a string and store it.
            natureName = (String)((Map<String, Object>)tempInner[enLocation]).get('name');
        }
        else
        {
            // NEED TO THROW ERROR OR SOMETHING HERE
            System.debug('Error recieved from HTTP response in getPokeNature');
            System.debug(response.getBody());
        }

        // return our stored value
        return natureName;
    }

    public Integer randNumUnderParam(Integer inCeiling)
    {
        // take our ceiling, subtract 1 for 0-indexing
        // then use Math.random() which returns between 0.0 and 1.0.
        // use that as a percent, and multiply by our (adjusted) ceiling
        // cast to Integer to truncate off Demical trailing numbers.
        
        return (Integer)(Math.random()*(inCeiling-1));
    }
}