public with sharing class PokeApiHelper {
    private enum pokeVar {NAME, NATURE, SPECIES, ABILITY, MOVE1, MOVE2, MOVE3, MOVE4}

    private static final Map<pokeVar, String> apiSetVars = new Map<pokeVar, String>{
        pokeVar.NAME => 'Name', pokeVar.Nature => 'Nature', pokeVar.SPECIES => 'Species',
        pokeVar.ABILITY => 'Ability', pokeVar.MOVE1 => 'Move1',
        pokeVar.MOVE2 => 'Move2', pokeVar.MOVE3 => 'Move3', pokeVar.MOVE4 => 'Move4'};

    public static void insertPokemon(List<String> ptaNames)
    {
        PokeApi accessor= new PokeApi();
        List<Pokemon__c> ptaObj=new List<Pokemon__c>();
        for(String poke : ptaNames)
        {
            Pokemon pikachu= accessor.getPokeByName(poke);
            if(pikachu!=null)
            {
                Pokemon__c record=new Pokemon__c();
                record.Name = pikachu.getPokeVal(apiSetVars.get(pokeVar.Species));
                record.Nature__c = pikachu.getPokeVal(apiSetVars.get(pokeVar.NATURE));
                record.Ability__c = pikachu.getPokeVal(apiSetVars.get(pokeVar.ABILITY));
                record.Move1__c = pikachu.getPokeVal(apiSetVars.get(pokeVar.MOVE1));
                record.Move2__c = pikachu.getPokeVal(apiSetVars.get(pokeVar.MOVE2));
                record.Move3__c = pikachu.getPokeVal(apiSetVars.get(pokeVar.MOVE3));
                record.Move4__c = pikachu.getPokeVal(apiSetVars.get(pokeVar.MOVE4));
                
                ptaObj.add(record);
            }
            else{System.debug('Error retrieving from PokeApi.class');}
        }
        try {
            insert ptaObj;
            for(Pokemon__c poke : ptaObj)
            {
                System.debug('pokemon id after insert: ' + poke.id );
            }
        } catch (Exception e) {
            System.debug('Error inserting Pokemon');
            System.debug(e.getMessage());
        }
    }
}