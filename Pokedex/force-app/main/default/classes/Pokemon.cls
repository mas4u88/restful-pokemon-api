public with sharing class Pokemon {

    
    public enum pokeVar {NAME, NATURE, SPECIES, ABILITY, MOVE1, MOVE2, MOVE3, MOVE4}
    public Map<pokeVar, String> myPokeVar = new Map<pokeVar, String>();
                
    private final Map<pokeVar, String> apiSetVars = new Map<pokeVar, String>{
        pokeVar.NAME => 'Name', pokeVar.Nature => 'Nature', pokeVar.SPECIES => 'Species',
        pokeVar.ABILITY => 'Ability', pokeVar.MOVE1 => 'Move1',
        pokeVar.MOVE2 => 'Move2', pokeVar.MOVE3 => 'Move3', pokeVar.MOVE4 => 'Move4'};

    //private static final PokeApi apiGetter = new PokeApi();

    public Pokemon() {}// leave default empty, used in other class

    public Pokemon(String inPokeSpecies)
    {
        this(inPokeSpecies, inPokeSpecies);
    }

    public Pokemon(String inPokeSpecies, String inPokeName) 
    {
        setPokeVal(pokeVar.SPECIES, inPokeSpecies);
        setPokeVal(pokeVar.NAME, inPokeName);
    }

    public void setPokeval(pokeVar inPokeVarEnum, String inValue)
    {
        setPokeVal(apiSetVars.get(inPokeVarEnum), inValue);
    }

    public void setPokeVal(String inPokeVar, String inValue)
    {
        
        if(inPokeVar == apiSetVars.get(pokeVar.NAME))
        {
            myPokeVar.put(pokeVar.NAME, inValue);
        } 
        else if (inPokeVar == apiSetVars.get(pokeVar.ABILITY))
        {
            myPokeVar.put(pokeVar.ABILITY, inValue);
        }
        else if(inPokeVar == apiSetVars.get(pokeVar.NATURE))
        {
            myPokeVar.put(pokeVar.NATURE, inValue);
        }
        else if (inPokeVar == apiSetVars.get(pokeVar.SPECIES))
        {
            myPokeVar.put(pokeVar.SPECIES, inValue);
        } 
        else if (inPokeVar == apiSetVars.get(pokeVar.MOVE1))
        {
            myPokeVar.put(pokeVar.MOVE1, inValue);
        } 
        else if (inPokeVar == apiSetVars.get(pokeVar.MOVE2))
        {
            myPokeVar.put(pokeVar.MOVE2, inValue);
        } 
        else if (inPokeVar == apiSetVars.get(pokeVar.MOVE3))
        {
            myPokeVar.put(pokeVar.MOVE3, inValue);
        } 
        else if (inPokeVar == apiSetVars.get(pokeVar.MOVE4))
        {
            myPokeVar.put(pokeVar.MOVE4, inValue);
        } 
    }
    public String getPokeVal(String inPokeVar)
    {
        if(inPokeVar == apiSetVars.get(pokeVar.NAME))
        {
            return myPokeVar.get(pokeVar.NAME);
        } 
        else if (inPokeVar == apiSetVars.get(pokeVar.ABILITY))
        {
            return myPokeVar.get(pokeVar.ABILITY);
        }
        else if(inPokeVar == apiSetVars.get(pokeVar.NATURE))
        {
            return myPokeVar.get(pokeVar.NATURE);
        }
        else if (inPokeVar == apiSetVars.get(pokeVar.SPECIES))
        {
            return myPokeVar.get(pokeVar.SPECIES);
        } 
        else if (inPokeVar == apiSetVars.get(pokeVar.MOVE1))
        {
            return myPokeVar.get(pokeVar.MOVE1);
        } 
        else if (inPokeVar == apiSetVars.get(pokeVar.MOVE2))
        {
            return myPokeVar.get(pokeVar.MOVE2);
        } 
        else if (inPokeVar == apiSetVars.get(pokeVar.MOVE3))
        {
            return myPokeVar.get(pokeVar.MOVE3);
        } 
        else if (inPokeVar == apiSetVars.get(pokeVar.MOVE4))
        {
            return myPokeVar.get(pokeVar.MOVE4);
        } 
        else
        {
            return null;
        }
    }
}